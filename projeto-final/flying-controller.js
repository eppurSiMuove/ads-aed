// elementos para modificar na tela
const displayLandings = document.getElementById("landings");
const displayDamageds = document.getElementById("damageds");
const myCanvas = document.getElementById("my-canvas");
const canvasContext = myCanvas.getContext("2d");

// dados para cálculos futuros
myCanvas.tenthPart = myCanvas.width * 0.1;
myCanvas.controlArea = myCanvas.width - 100;
let controlAreaList = [];

// estilo da fonte - combustível
canvasContext.font = "13px sans-serif";

// imagem do avião
const airplaneImg = new Image();
airplaneImg.src = './my-airplane.png';

// variáveis de estado
const controllerState = {
  landedPlanes: 0,
  damagedPlanes: 0,
  runningTime: 0
};

// classe para instanciar os aviões
class Airplane {
  static counter = 0;

  // propriedades principais
  constructor(fuel_amount, x_pos, y_pos, plane_id) {
    this.id = plane_id;
    this.fuel = fuel_amount;     // algo entre 3 e 10 unidades
    this.x = x_pos;              // algo entre 0  e 50% do canvas
    this.y = y_pos;              // algo entre topo e o fundo do canvas
    this.w = 60;
    this.h = 50;
    this.dist = 0;
    this.hidden = false;
    this.inControlArea = false;
    this.image = airplaneImg;
  }
  
  // move no eixo x
  move(){
    this.x += (11 - this.fuel) * 2;
    this.dist += (11 - this.fuel) * 2;
  };

  // diminui o combustível
  consumeGas() {
    if (this.dist >= myCanvas.tenthPart) {
      this.dist = 0;
      if (this.fuel <= 3) {
        this.fuel -= 0.2;
      } else {
        this.fuel--;
      };
    };
  };

  // posição do avião relativo ao canvas em porcentagem
  relativePosition() {
    return Math.floor(this.x * 100 / myCanvas.width);
  };
};

// array com todos os aviões do jogo - melhor dentro de controllerState?
let planes = [];

// multiplicação tem precedência =]
function randomIntFromInterval(min, max){
  return Math.floor(Math.random() * (max - min + 1) + min);
};

// função para construir os aviões dinâmicamente
function arrivingPlanes() {
  const randomNumber = randomIntFromInterval(0, 3);

  for (let i = randomNumber; i >= 0; i--) {
    const fuel = randomIntFromInterval(3, 10);
    const x_axis = randomIntFromInterval(0, myCanvas.width * 0.5);
    const y_axis = randomIntFromInterval(0, myCanvas.height - 50);
    const id = Airplane.counter++;

    let a = new Airplane(fuel, x_axis, y_axis, id);
    planes[planes.length] = a;
  };
};

// função principal
function executeController() {
  canvasContext.clearRect(0, 0, myCanvas.width, myCanvas.height);

  // itera a array de aviões
  for (let i = 0; i < planes.length; i++) {
    let a = planes[i];

    if (!a.hidden) {

      // avião "pousado"
      if (a.x >= myCanvas.width && a.fuel > 0) {
        a.hidden = true;
        a.fuel = 10;
        displayLandings.innerText = ++controllerState.landedPlanes;

      // avião "danificado"
      } else if (a.x < myCanvas.width && a.fuel <= 0) {
        a.hidden = true;
        a.fuel = 10;
        displayDamageds.innerText = ++controllerState.damagedPlanes;

      } else {
        // controle de chegada serial
        if (a.x >= myCanvas.controlArea) {
          if (!a.inControlArea) {
            a.inControlArea = true;
            controlAreaList[controlAreaList.length] = a;
          };

          // dá prioridade ao avião com menos combustível na 'controlArea'
          controlAreaList.sort((a, b) => a.fuel - b.fuel);
          const priorityPlane = controlAreaList[0];
          planes[priorityPlane.id].move();
          planes[priorityPlane.id].consumeGas();
        };

        // move o aviao atual e calcula gasto de combustível
        a.move();
        a.consumeGas();

        // desenho no canvas
        canvasContext.drawImage(a.image, a.x, a.y, a.w, a.h);
        canvasContext.fillStyle = '#C2C2C2';
        canvasContext.fillText(a.fuel.toFixed(2), a.x + 25, a.y + 10);
      };
    };
  };

  // define o ciclo de chegada de mais aviões na tela
  controllerState.runningTime += 100;
  if (controllerState.runningTime % 2000 === 0) {
    arrivingPlanes();
  };

  // chama a si mesmo a cada 100 ms
  setTimeout(executeController, 100);
};

// inicia o controlador de vôo
executeController();
