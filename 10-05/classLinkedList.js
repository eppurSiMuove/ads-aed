class LinkedQueue {
  constructor(items) {
    this.head = {
      current: undefined,
      next: undefined,
    };
    this.last = this.head;
  };

  add(item) {
    this.last.next = {
      current: item,
      next: undefined,
    };
    this.last = this.last.next;
  };

  rm() {
    this.head = this.head.next;
    return this.head.current;
  };

  size() {
  };

  isEmpty() {
  };

  forEach(fn) {
    let item = this.head;
    while (item.next != undefined) {
      fn(item.current);
      item = item.next;
    };
    fn(item.current);
  };
};

let bankQueue = new LinkedQueue();

/*
bankQueue.add('Andre');
bankQueue.add('Camila');
bankQueue.add('Felipe');
bankQueue.add('Luana');

bankQueue.rm();

function checkDocument(person) {
  if (person == 'Felipe') {
    console.log(person, 'your docs are ok.');
  } else {
    console.log(person, 'there is some docs missing.');
  };
};

function greetings(person) {
  console.log("Hey,", person, ", how are you doing?");
};

bankQueue.forEach(checkDocument);
bankQueue.forEach(greetings);

/*
class Stack {

  constructor(items) {
    this.stackItems = items || [];
  };

  add(item) {
    this.stackItems[this.stackItems.length] = item;
  };

  rm() {
    let removedItem = this.stackItems[this.stackItems.length];
    this.stackItems.length = this.stackItems.length - 1;
    return removedItem;
  };

  size() {
    return this.stackItems.length;
  };

  isEmpty() {
    return this.stackItems.length > 0 ? false : true;
  };
};

let myStack = new Stack(['zero', 'dotFive']);
console.log('Is the queue empty? It should be false: ', myStack.isEmpty());
console.log('What is the queue size? It should be two: ', myStack.size());

myStack.add('one');
myStack.add('two');
myStack.add('three');
console.log('Is the queue empty? It should be false: ', myStack.isEmpty());
console.log('What is the queue size? It should be five: ', myStack.size());

myStack.rm();
myStack.rm();
console.log('Is the queue empty? It should be false: ', myStack.isEmpty());
console.log('What is the queue size? It should be three: ', myStack.size());

console.log(myStack);
*/
