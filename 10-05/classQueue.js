class Queue {
  constructor(items) {
    this.queueItems = items || [];
  };

  add(item) {
    this.queueItems[this.queueItems.length] = item;
  };

  rm() {
    let removedItem = this.queueItems[0];
    for (let i = 0; i < this.queueItems.length - 1; i++) {
      this.queueItems[i] = this.queueItems[i+1];
    };
    this.queueItems.length = this.queueItems.length - 1;
    return removedItem;
  };

  size() {
    return this.queueItems.length;
  };

  isEmpty() {
    return this.queueItems.length > 0 ? false : true;
  };

  forEach(fn) {
    for (let i = 0; i < this.queueItems.length; i++) {
      fn(this.queueItems[i]);
    };
  };
};

let bankQueue = new Queue();
console.log('Is the queue empty? It should be true: ', bankQueue.isEmpty());
console.log('What is the queue size? It should be zero: ', bankQueue.size());
bankQueue.add('Andre');
bankQueue.add('Felipe');

function checkDocument(person) {
  if (person == 'Felipe') {
    console.log(person, 'your docs are ok.');
  } else {
    console.log(person, 'there is some docs missing.');
  };
};

function greetings(person) {
  console.log("Hey,", person, ", how are you doing?");
};

bankQueue.forEach(checkDocument);
bankQueue.forEach(greetings);

/*
class Stack {

  constructor(items) {
    this.stackItems = items || [];
  };

  add(item) {
    this.stackItems[this.stackItems.length] = item;
  };

  rm() {
    let removedItem = this.stackItems[this.stackItems.length];
    this.stackItems.length = this.stackItems.length - 1;
    return removedItem;
  };

  size() {
    return this.stackItems.length;
  };

  isEmpty() {
    return this.stackItems.length > 0 ? false : true;
  };
};

let myStack = new Stack(['zero', 'dotFive']);
console.log('Is the queue empty? It should be false: ', myStack.isEmpty());
console.log('What is the queue size? It should be two: ', myStack.size());

myStack.add('one');
myStack.add('two');
myStack.add('three');
console.log('Is the queue empty? It should be false: ', myStack.isEmpty());
console.log('What is the queue size? It should be five: ', myStack.size());

myStack.rm();
myStack.rm();
console.log('Is the queue empty? It should be false: ', myStack.isEmpty());
console.log('What is the queue size? It should be three: ', myStack.size());

console.log(myStack);
*/
