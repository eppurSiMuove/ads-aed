/*
console.log('Class was called');

class People {
  constructor(name, age) {
    this.name = name;
    this.age = age;
  }

  showName() {
    return this.name;
  };
};

let andre = new People('André', 33);
console.log('Meu nome é ', andre.showName());

class Queue {

  constructor(items) {
    this.queueItems = items || [];
  };

  add(item) {
    this.queueItems[this.queueItems.length] = item;
  };

  rm() {
    let removedItem = this.queueItems[0];
    for (let i = 0; i < this.queueItems.length - 1; i++) {
      this.queueItems[i] = this.queueItems[i+1];
    };
    this.queueItems.length = this.queueItems.length - 1;
    return removedItem;
  };

  size() {
    return this.queueItems.length;
  };

  isEmpty() {
    return this.queueItems.length > 0 ? false : true;
  };
};

let myQueue = new Queue();
console.log('Is the queue empty? It should be true: ', myQueue.isEmpty());
console.log('What is the queue size? It should be zero: ', myQueue.size());
myQueue.add('one');
myQueue.add('two');
myQueue.add('three');
console.log('Is the queue empty? It should be false: ', myQueue.isEmpty());
console.log('What is the queue size? It should be three: ', myQueue.size());
myQueue.rm();
console.log('Is the queue empty? It should be false: ', myQueue.isEmpty());
console.log('What is the queue size? It should be two: ', myQueue.size());
*/

class Stack {

  constructor(items) {
    this.stackItems = items || [];
  };

  add(item) {
    this.stackItems[this.stackItems.length] = item;
  };

  rm() {
    let removedItem = this.stackItems[this.stackItems.length];
    this.stackItems.length = this.stackItems.length - 1;
    return removedItem;
  };

  size() {
    return this.stackItems.length;
  };

  isEmpty() {
    return this.stackItems.length > 0 ? false : true;
  };
};

let myStack = new Stack(['zero', 'dotFive']);
console.log('Is the queue empty? It should be false: ', myStack.isEmpty());
console.log('What is the queue size? It should be two: ', myStack.size());

myStack.add('one');
myStack.add('two');
myStack.add('three');
console.log('Is the queue empty? It should be false: ', myStack.isEmpty());
console.log('What is the queue size? It should be five: ', myStack.size());

myStack.rm();
myStack.rm();
console.log('Is the queue empty? It should be false: ', myStack.isEmpty());
console.log('What is the queue size? It should be three: ', myStack.size());

console.log(myStack);
