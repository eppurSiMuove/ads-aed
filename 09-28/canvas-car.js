let car = new Image();
car.src = './my-car.png';

let direction = 1;
let startPoint = 10;
let myCanvas = document.getElementById('my-canvas');
let canvasContext = myCanvas.getContext('2d');
canvasContext.fillStyle = 'green';

setInterval(() => {
  canvasContext.clearRect(0, 0, myCanvas.width, myCanvas.height);
  // canvasContext.fillRect(startPoint, 10, 100, 100);
  canvasContext.drawImage(car, startPoint, 10);
  if (startPoint > myCanvas.width) {
    startPoint = 0;
  };
  startPoint = startPoint + (10 * direction);
}, 20);

document.addEventListener('keydown', function(event) {
  if (event.key == 'ArrowLeft') {
    direction = -1;
  };
  if (event.key == 'ArrowRight') {
    direction = 1;
  };
  console.log(event.key);
});
