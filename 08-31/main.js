const read = require('../readstdin.js');

/*
let n = parseInt(read("Type some integer: "));

switch (n) {
  case 1:
    console.log("You chose number ", n);
    break;
  case 2:
    console.log("You chose number ", n);
    break;
  case 3:
    console.log("This is more than 2");
    break;
  default:
    console.log("Its a negative number or maybe a non number");
    break;
}
*/

let pao = {
  pesoEmGramas: 30,
  nome: "Pão de sal",
  codigoDeBarras: "123",
  preco: 18.9 / (1000/30),
  obs: "Cada pão no peso",
  quantidade: 5,
};

let cerveja = {
  nome: "Original",
  preco: 11.00,
  codigoDeBarras: "1234",
  obs: "Garrafa 600ml",
  quantidade: 12,
};

let precoTotal = 0;
let carrinhoDeCompras = [pao, cerveja];
for(let i = 0; i < carrinhoDeCompras.length; i++){
  let qtd = carrinhoDeCompras[i].quantidade;
  let nome = carrinhoDeCompras[i].nome;
  let precoItens = qtd * carrinhoDeCompras[i].preco;
  console.log(qtd, 'x ', nome , ' = R$ ', precoItens);
  precoTotal += precoItens;
};

console.log('Valor total: R$ ' + precoTotal);

