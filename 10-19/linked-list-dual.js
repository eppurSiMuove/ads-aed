
class LinkedQueueDual {
  constructor(items) {
    this.head = {
      next: undefined,
      current: undefined,
      prev: undefined,
    };
    this.last = this.head;
  };

  add(item) {
    this.last.next = {
      next: undefined,
      current: item,
      prev: this.last,
    };
    this.last = this.last.next;
  };

  rm() {
    this.head = this.head.next;
    return this.head.current;
  };

  rmItem(item) {
    console.log(item.current);
    item.prev.next = item.next;
    item.next.prev = item.prev;
  }

  size() {
  };

  isEmpty() {
  };

  forEach(fn) {
  };
};

let myllDual = new LinkedQueueDual();
myllDual.add("Andre");
myllDual.add("Felipe");
myllDual.add("Amaral");
myllDual.add("Siqueira");
console.log(myllDual);
