let gameState = {
  cars: [],
  fps: 1000/60,
  pause: false,
};

let car = new Image();
car.src = './my-car.png';

let direction = 1;
let myCanvas = document.getElementById('my-canvas');
let canvasContext = myCanvas.getContext('2d');

let carOne = { x: Math.random() * 300, startPoint: Math.random() * 300, direction: 1};
let carTwo = { x: Math.random() * 400, startPoint: Math.random() * 300, direction: -1};
let carLast = { x: Math.random() * 600, startPoint: Math.random() * 300, direction: 1};

let cars = [carOne, carTwo, carLast];

setInterval(() => {
  if (gameState.pause) return;
  canvasContext.clearRect(0, 0, myCanvas.width, myCanvas.height);
  for (let i = 0; i < cars.length; i++) {
    canvasContext.drawImage(car, cars[i].startPoint, cars[i].x, 135, 50);
    if (cars[i].startPoint > myCanvas.width) {
      cars[i].startPoint = 0;
    };
    if (cars[i].startPoint < 0) {
      cars[i].startPoint = myCanvas.width;
    };
    cars[i].startPoint = cars[i].startPoint + cars[i].direction;
  };
}, 10);

document.addEventListener('keydown', function(event) {
  switch (event.key) {
    case 'ArrowRight':
    case 'ArrowLeft':
      for (let i = 0; i < cars.length; i++) {
        cars[i].direction *= -1;
      }
      break;

    case 'N':
    case 'n':
      let directionDelta = Math.random();
      let carDirection = directionDelta > 0.5 ? 1 : -1;
      let arrivingCar = { x: (Math.random() * 600), startPoint: (Math.random() * 300), direction: carDirection };
      cars[cars.length] = arrivingCar;
      break;
  }
  console.log(event.key);
});
