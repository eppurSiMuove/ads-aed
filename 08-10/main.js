/*
var nome = "André";
var idade = 34;
let idade_esposa = 21;

console.log("Minha idade é " + idade);
console.log("A idade da minha esposa é " + idade_esposa);

let pessoas = ["Gustavo", "Tânia", "José"];

console.log(pessoas[1, 2]);

console.log(pessoas[pessoas.length - 1]);

for (let i = 0; i < pessoas.length; i++) {
  console.log("pessoas["+ i +"] = " + pessoas[i]);
};

for (let k = 10; k >= 0; k--) {
  console.log(k);
};
*/

const fs = require("fs");
let stdin = fs.openSync("/dev/stdin","rs");

const read = function(message) {
  fs.writeSync(process.stdout.fd, message + " ");
  let s = '';
  let buf = Buffer.alloc(1);
  fs.readSync(stdin,buf,0,1,null);
  while((buf[0] != 10) && (buf[0] != 13)) {
    s += buf;
    fs.readSync(stdin,buf,0,1,null);
  }
  return s;
}
/*
let idade = parseInt(read("Digite sua idade"));
let nome = read("Digite o seu nome");

if (idade > 30) {
  console.log(nome + ", vc está velho(a).");
} else if (idade >= 18) {
  console.log(nome + ", vc pode beber vodka.");
} else {
  console.log(nome + ", vc é criança... vai para casa.");
};


let num1 = parseInt(read("Digite o primeiro número"));
let num2 = parseInt(read("Digite o segundo número"));

if (num1 > num2) {
  console.log("O maior número é " + num1);
} else {
  console.log("O maior número é " + num2);
};
*/

let numero = 0;
let maior = -9999999999999;
for (let c = 1; c < 11; c++){
  numero = parseInt(read("Digite o " + c + "º número:"));
  if (numero > maior) {
    maior = numero;
  };
};
console.log("Dentre os números informados o maior foi: " + maior);

