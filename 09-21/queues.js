let fila = [];

function add(queue, value) {
  queue[queue.length] = value;
};

function rm(queue) {
  let ret = queue[0];
  for (let i = 0; i < queue.length - 1; i++) {
    queue[i] = queue[i+1];
  };
  queue.length = queue.length - 1;
  return ret;
};

add(fila, 10);
add(fila, 20);
add(fila, 1);
console.log(fila);

console.log(rm(fila), fila);
