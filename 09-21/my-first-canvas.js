let startPoint = 10;
let myCanvas = document.getElementById('my-canvas');
let canvasContext = myCanvas.getContext('2d');
canvasContext.fillStyle = 'green';

setInterval(() => {
  canvasContext.clearRect(0, 0, myCanvas.width, myCanvas.height);
  canvasContext.fillRect(startPoint, 10, 100, 100);
  if (startPoint > myCanvas.width) {
    startPoint = 0;
  };
  startPoint = startPoint + 10;
}, 20);
