const read = require('../readstdin.js');
// Aula de 31 de Agosto de 2023

let pao = {
  pesoEmGramas: 30,
  nome: "Pão de sal",
  codigoDeBarras: "123",
  preco: 18.9 / (1000 / 30),
  obs: "Cada pão no peso",
  quantidade: 5,
};

let cerveja = {
  nome: "Original",
  preco: 11.00,
  codigoDeBarras: "1234",
  obs: "Garrafa 600ml",
  quantidade: 12,
};

let carrinhoDeCompras = [pao, cerveja];

let precoTotal = 0;
for (let i = 0; i < carrinhoDeCompras.length; i++) {
  let qtd = carrinhoDeCompras[i].quantidade;
  let nome = carrinhoDeCompras[i].nome;
  let precoItens = qtd * carrinhoDeCompras[i].preco;
  console.log(qtd, 'x ', nome, ' = R$ ', precoItens);
  precoTotal += precoItens;
};
console.log('Valor total: R$ ' + precoTotal);

// Aula de 01 de Setembro de 2023

// Create a function to show the most expensive product in the cart
function mostExpensiveProd(cart) {
  let expensiveProd = cart[0].nome;
  let expensiveValue = cart[0].preco;
  for (let i = 1; i < cart.length; i++) {
    if (cart[i].preco > expensiveValue) {
      expensiveValue = cart[i].preco;
      expensiveProd = cart[i].nome;
    };
  };
  return expensiveProd;
};
console.log("O produto mais caro no carrinho é: ", mostExpensiveProd(carrinhoDeCompras));

/*
 TODO:
  00 -> Create a function to show the total product quantity
  01 -> Create a function to show the product with most items in the cart
  02 -> Create a function to tell the return of the payment
  03 -> Create a function to add a product to the carrinhoDeCompras.
  04 -> Create a function to remove a product from carrinhoDeCompras.
  05 -> Create a function to update an item in the carrinhoDeCompras.
  06 -> Create a function to print a bill.
  07 -> Create a function to split the bill among `N` people.
  08 -> Create a function the will remove items from the carrinhoDeCompras to fit the money provided by the buyer.
  09 -> Create a function to print all products sorted by name.
  10 -> Put all your code in your git repository and send it to the teacher.
*/

// 00 -> Create a function to show the total product quantity
function totalItems(cart) {
  let items = 0;
  for (let i = 0; i < cart.length; i++) {
    items += cart[i].quantidade;
  };
  return items;
};
console.log("Quantidade total de itens no carrinho = ", totalItems(carrinhoDeCompras));

//  01 -> Create a function to show the product with most items in the cart
function mostNumerousProd(cart) {
  let numerousProd = cart[0].nome;
  let numerousQty = cart[0].quantidade;
  for (let i = 1; i < cart.length; i++) {
    if (cart[i].quantidade > numerousQty) {
      numerousQty = cart[i].quantidade;
      numerousProd = cart[i].nome;
    };
  };
  return numerousProd;
};
console.log("O produto mais numeroso no carrinho é: ", mostNumerousProd(carrinhoDeCompras));

//  02 -> Create a function to tell the return of the payment
function paymentReturn(cart, totalGiven) {
  let total = 0;
  for (let i = 0; i < cart.length; i++) {
    let itemsProdValue = cart[i].preco * cart[i].quantidade;
    total += itemsProdValue;
  };
  return (totalGiven > total) ? (totalGiven - total) : 0;
};
let payment = parseFloat(read("Dar quantos reais de pagamento? : "));
console.log("O troco para R$ ", payment, " pila é: ", paymentReturn(carrinhoDeCompras, payment));

//  03 -> Create a function to add a product to the carrinhoDeCompras.
function addProduct(cart, nome, codigoDeBarras, quantidade, preco, obs) {
  let newProduct = { nome, codigoDeBarras, quantidade, preco, obs };
  carrinhoDeCompras[cart.length] = newProduct;
};
addProduct(carrinhoDeCompras, "Empada de Frango", 37829348, 1, 4.5, "a melhor do Brasil");
addProduct(carrinhoDeCompras, "Torta de Limao", 10826341, 2, 14.25, "serve 3 pessoas");
console.log(carrinhoDeCompras);

//  04 -> Create a function to remove a product from carrinhoDeCompras.
function removeProduct(cart, nome) {
  for (let i = 0; i < cart.length; i++) {
    if (cart[i].nome == nome) {
      cart.splice(i, 1);
      break;
    };
  };
};
removeProduct(carrinhoDeCompras, 'Pão de sal');
console.log('Novo carrinho:');
console.log(carrinhoDeCompras);

//  05 -> Create a function to update an item in the carrinhoDeCompras.
function updateItems(cart, nome, quantidade) {
  for (let i = 0; i < cart.length; i++) {
    if (cart[i].nome == nome) {
      if (quantidade > 0) {
        cart[i].quantidade = quantidade;
      } else {
        cart.splice(i, 1);
      };
      break;
    };
  };
};
updateItems(carrinhoDeCompras, 'Empada de Frango', 4);
console.log('Novo carrinho:');
console.log(carrinhoDeCompras);

//  06 -> Create a function to print a bill.
function printBill(cart) {
  let precoTotal = 0;
  for (let i = 0; i < cart.length; i++) {
    let precoItem = cart[i].quantidade * cart[i].preco;
    console.log(cart[i].quantidade, ' x ', cart[i].nome, ' = R$ ', precoItem);
    precoTotal += precoItem;
  };
  console.log('Valor total do carrinho: R$ ', precoTotal);
};
printBill(carrinhoDeCompras);

//  07 -> Create a function to split the bill among `N` people.
function splitBill(cart, payers) {
  let precoTotal = 0;
  for (let i = 0; i < cart.length; i++) {
    let precoItem = cart[i].quantidade * cart[i].preco;
    precoTotal += precoItem;
  };
  console.log('Valor total dividido para', payers, 'pessoas: R$ ', precoTotal / payers);
};
splitBill(carrinhoDeCompras, 5);

//  08 -> Create a function the will remove items from the carrinhoDeCompras to fit the money provided by the buyer.
function adjustCartToFitPayment(shopCart, payment) {
  let totalPrice = 0;
  for (let i = 0; i < shopCart.length; i++) {
    let itemPrice = shopCart[i].quantidade * shopCart[i].preco;
    totalPrice += itemPrice;
  };
  console.log('Payment = R$', payment, ' and Total Price = R$ ', totalPrice);
  if (payment < totalPrice) {
    let itemToAdjust = read('Payment value under total value. Which product do you want to adjust so your payment fit total value? : ');
    let msg = 'How many ' + itemToAdjust + ' do want to have in your cart? : ';
    let quantity = parseInt(read(msg));
    updateItems(shopCart, itemToAdjust, quantity);
    console.log(shopCart);
    return adjustCartToFitPayment(shopCart, payment);
  } else {
    return payment > totalPrice ? payment - totalPrice : 0;
  };
};
let moneyYouHave = parseFloat(read('How many money do you have available to pay the bill? R$ '));
let returnedToYou = adjustCartToFitPayment(carrinhoDeCompras, moneyYouHave);
console.log('You give me R$ ', moneyYouHave, '. I\'ve returned to you R$ ', returnedToYou);

//  09 -> Create a function to print all products sorted by name.
addProduct(carrinhoDeCompras, 'Broa de Milho', 32536, 15, 1.25, 'askf');
addProduct(carrinhoDeCompras, 'Suco de Laranja', 09765, 1, 5.5, '500ml');
addProduct(carrinhoDeCompras, 'Pastel com Carne e Queijo', 32563221, 3, 3.49, 'aslkfkja');
addProduct(carrinhoDeCompras, 'Café expresso', 2352245, 2, 2.39, '100ml');
function sortProducts(cart) {
  let len = cart.length;
  let aux = {};
  for (let i = 1; i < len - 1; i++) {
    for (let k = 0; k < len - i; k++) {
      if (cart[k].nome[0] > cart[k + 1].nome[0]) {
        aux = cart[k];
        cart[k] = cart[k + 1];
        cart[k + 1] = aux;
      };
    };
  };
  console.log(cart);
};
sortProducts(carrinhoDeCompras);
