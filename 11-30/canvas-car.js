let db = new NunDb('wss://ws-staging.nundb.org/', 'class-game', 'class-game-token');

let playerName = localStorage.getItem('playerName');
if (!playerName) {
  playerName = prompt("What's your name?");
  localStorage.setItem('playerName', playerName);
};
document.getElementById("car-name").value = playerName;

let gameState = {
  cars: [],
  fps: 1000/10,
  pause: false,
  playerName: playerName,
};

let car = new Image();
car.src = './my-car.png';

let direction = 1;
let myCanvas = document.getElementById('my-canvas');
let canvasContext = myCanvas.getContext('2d');
canvasContext.font = "16px sans-serif";

let carOne = { x: Math.random() * 300, startPoint: Math.random() * 300, direction: 1, name: playerName};

let cars = [ ];
let carsMap = { };

async function startGame() {
  let dbCars = (await db.get('cars')).value;
  console.log(dbCars);
  for (let carName in dbCars) {
    let car = dbCars[carName];
    carsMap[car.name] = car;
    cars.push(car);
  };

  if(!carsMap[carOne.name]) {
    carsMap[carOne.name] = carOne;
    cars.push(carOne);
  }

  setTimeout(gameLoop, gameState.fps);
  dbCars[playerName] = carOne;
  db.set('cars', dbCars);
  db.watch('lastAction', function(e) {
    applyAction(e.value.player, e.value.key);
  });
  db.watch('cars', function(e) {
    let commingCars = e.value;
    let players = Object.keys(commingCars);
    for (let i = 0; i < players.length; i++) {
      let playerExists = carsMap[players[i]];
      if(!playerExists) {
        carsMap[players[i]] = commingCars[players[i]];
        cars.push(carsMap[players[i]]);
      }
    }
  });
};

function gameLoop() {
  if (gameState.pause) return;
  canvasContext.clearRect(0, 0, myCanvas.width, myCanvas.height);
  for (let i = 0; i < cars.length; i++) {
    canvasContext.drawImage(car, cars[i].startPoint - 25, cars[i].x + 5, 72.5, 20);
    canvasContext.fillText(cars[i].name, cars[i].startPoint, cars[i].x);
    if (cars[i].startPoint > myCanvas.width) {
      cars[i].startPoint = 0;
    };
    if (cars[i].startPoint < 0) {
      cars[i].startPoint = myCanvas.width;
    };
    cars[i].startPoint = cars[i].startPoint + cars[i].direction * 10;
  };

  setTimeout(gameLoop, gameState.fps);
};


function cleanStorage() {
  localStorage.removeItem('playerName');
  window.location.reload();
};

document.addEventListener('keydown', function(event) {
  let selectedCar = document.getElementById('car-name').value;
  applyAction(selectedCar, event.key, true);
});

function applyAction(carName, key, replicate) {
  let mappedCar = carsMap[carName];
  if (!mappedCar) {
    console.log("Car not found!");
  };

  switch (key) {
    case 'ArrowRight':
      mappedCar.direction = 1;
      if(replicate){
        db.set("lastAction", {key: key, player: carName});
      };
      break;
    case 'ArrowLeft':
      mappedCar.direction = -1;
      if(replicate){
        db.set("lastAction", {key: key, player: carName});
      };
      break;
  }
}

startGame();
