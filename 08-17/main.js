const read = require('../readstdin.js');

//for (let i = 0; i <= 10; i++){
//  if (i % 2 == 0) {
//    console.log(i);
//  };
//};

/*
function ePar(num) {
  if (num % 2 == 0) {
    return true;
  };
  return false;
};

let x = read("Digite um número: ");
console.log(ePar(x));

function from0To100(){
  for (let k = 1; k <= 2; k++){
    for (let i = 0; i < 101; i++) {
      console.log(i);
    };
  };
};

from0To100();

function sumAndDiff(n1, n2) {
  console.log("Soma:", n1 + n2);
  console.log("Diferença:", n1 - n2);
};
sumAndDiff(parseInt(read("Digite primeiro número: ")), parseInt(read("Digite segundo número: ")));

function doubleAgedMother(ageAtLabor){
  let childAge = ageAtLabor;
  return childAge * 2;
};
console.log(doubleAgedMother(parseInt(read("Nome da mãe: "))))

function returnOddNumbers(){
  for (let k = 1; k <= 5; k++){
    let n = parseInt(read("Digite o " + k + " número: "));
    if (n % 2 != 0) {
      console.log(n + " is odd.");
    };
  };
};
returnOddNumbers();
*/

function returnPrimeNumber() {
  for (let k = 1; k <= 10; k++){
    let n = parseInt(read("Digite o " + k + "º número: "));
    let counter = 0;
    for (let i = 2; i <= n/2; i++) {
      n % i == 0 ? counter++ : counter;
    };
    console.log(counter == 0 ? n + " is prime.\n" : "");
  };
};
returnPrimeNumber();
